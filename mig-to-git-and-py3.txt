200507 cet 

refs:
    https://docs.python.org/3/library/venv.html
    https://github.com/frej/fast-export
    https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket
        Update April 21, 2020] Due to the impact of COVID-19, we are extending the
        deprecation date by 30 days to give you more time to transition. Mercurial
        features and repositories will be officially removed from Bitbucket and its
        API 2020 July 1, 2020.

dir struct::

    text-hr
        text-hr-hg

setup::

    git clone git@github.com:frej/fast-export.git

    mkdir text-hr
    cd text-hr
    git init 

    python3 -m venv py-txthr
    . py-txthr/bin/activate
    pip install mercurial
    pip install --upgrade pip
    export PYTHON=$(which python)

migrate::

    ../fast-export/hg-fast-export.sh -r ../text-hr-hg --force


log::

    ../fast-export/hg-fast-export.py:269: FutureWarning: Possible nested set at position 2
      p=re.compile(b'([[ ~^:?\\\\*]|\.\.)')
    master: Exporting full revision 1/27 with 42/0/0 added/changed/removed files
    master: Exporting simple delta revision 2/27 with 1/2/0 added/changed/removed files
    master: Exporting simple delta revision 3/27 with 0/1/1 added/changed/removed files
    master: Exporting simple delta revision 4/27 with 0/24/0 added/changed/removed files
    master: Exporting simple delta revision 5/27 with 0/9/0 added/changed/removed files
    master: Exporting simple delta revision 6/27 with 0/3/0 added/changed/removed files
    master: Exporting simple delta revision 7/27 with 1/2/0 added/changed/removed files
    master: Exporting simple delta revision 8/27 with 0/1/0 added/changed/removed files
    master: Exporting simple delta revision 9/27 with 0/1/0 added/changed/removed files
    master: Exporting simple delta revision 10/27 with 0/2/0 added/changed/removed files
    master: Exporting simple delta revision 11/27 with 0/7/0 added/changed/removed files
    master: Exporting simple delta revision 12/27 with 0/1/0 added/changed/removed files
    master: Exporting simple delta revision 13/27 with 0/5/1 added/changed/removed files
    master: Exporting simple delta revision 14/27 with 1/3/0 added/changed/removed files
    master: Exporting simple delta revision 15/27 with 0/6/0 added/changed/removed files
    master: Exporting simple delta revision 16/27 with 0/2/0 added/changed/removed files
    master: Exporting simple delta revision 17/27 with 0/3/0 added/changed/removed files
    master: Exporting simple delta revision 18/27 with 0/4/0 added/changed/removed files
    master: Exporting simple delta revision 19/27 with 0/2/0 added/changed/removed files
    master: Exporting simple delta revision 20/27 with 0/1/0 added/changed/removed files
    master: Exporting simple delta revision 21/27 with 1/0/0 added/changed/removed files
    master: Exporting simple delta revision 22/27 with 1/3/1 added/changed/removed files
    master: Exporting simple delta revision 23/27 with 0/2/0 added/changed/removed files
    master: Exporting simple delta revision 24/27 with 1/0/1 added/changed/removed files
    master: Exporting simple delta revision 25/27 with 0/2/0 added/changed/removed files
    master: Exporting simple delta revision 26/27 with 0/3/0 added/changed/removed files
    master: Exporting simple delta revision 27/27 with 0/1/0 added/changed/removed files
    Issued 27 commands
    git-fast-import statistics:
    ---------------------------------------------------------------------
    Alloc'd objects:       5000
    Total objects:          205 (         4 duplicates                  )
          blobs  :          135 (         3 duplicates         83 deltas of        134 attempts)
          trees  :           43 (         1 duplicates         37 deltas of         40 attempts)
          commits:           27 (         0 duplicates          0 deltas of          0 attempts)
          tags   :            0 (         0 duplicates          0 deltas of          0 attempts)
    Total branches:           1 (         1 loads     )
          marks:           1024 (        27 unique    )
          atoms:             49
    Memory total:          2344 KiB
           pools:          2110 KiB
         objects:           234 KiB
    ---------------------------------------------------------------------
    pack_report: getpagesize()            =       4096
    pack_report: core.packedGitWindowSize = 1073741824
    pack_report: core.packedGitLimit      = 8589934592
    pack_report: pack_used_ctr            =          2
    pack_report: pack_mmap_calls          =          1
    pack_report: pack_open_windows        =          1 /          1
    pack_report: pack_mapped              =    1591711 /    1591711
    ---------------------------------------------------------------------


tests::

    cd ~/env/text-hr/text-hr
    export PYTHONPATH=~/env/text-hr/text-hr
    cd tests/
    python2.7 tests.py

python 3 setup::

    cd ~/env/text-hr
    python3 -m venv py-txthr
    . py-txthr/bin/activate
    pip install --upgrade pip


migration to python 3::

    git st # no changes

    2to3-3.7 -w -n <some-file>
    #      -w, --write           Write back modified files
    #      -n, --nobackups       Don't write backups for modified files

    python -c"import text_hr"


TODO:

    RefactoringTool: Warnings/messages while refactoring:
    RefactoringTool: ### In file vocals.py ###
    RefactoringTool: Line 134: You should use a for loop here
        list(map(partial(add_category, category=OTVORNICI), ("i", "e", "a", "o", "u", "ie")))
    RefactoringTool: Line 137: You should use a for loop here
    RefactoringTool: Line 140: You should use a for loop here
    RefactoringTool: Line 143: You should use a for loop here
    RefactoringTool: Line 147: You should use a for loop here
    RefactoringTool: Line 150: You should use a for loop here
    RefactoringTool: Line 154: You should use a for loop here
    RefactoringTool: Line 157: You should use a for loop here
    RefactoringTool: Line 160: You should use a for loop here
    RefactoringTool: Line 168: You should use a for loop here
    RefactoringTool: Line 172: You should use a for loop here
    RefactoringTool: Line 176: You should use a for loop here
    RefactoringTool: Line 181: You should use a for loop here
    RefactoringTool: Line 184: You should use a for loop here
    RefactoringTool: Line 188: You should use a for loop here
    RefactoringTool: Line 193: You should use a for loop here
    RefactoringTool: Line 195: You should use a for loop here
    RefactoringTool: Line 197: You should use a for loop here
    RefactoringTool: Line 199: You should use a for loop here
    RefactoringTool: Line 201: You should use a for loop here
    RefactoringTool: Line 203: You should use a for loop here
    RefactoringTool: Line 205: You should use a for loop here
    RefactoringTool: Line 208: You should use a for loop here

    tests/runall.txt
    tests/test_adj.txt
    tests/test_adj_wts.txt
    tests/test_detect.txt
    tests/test_nouns-a-m0.txt
    tests/test_nouns-a-moe.txt
    tests/test_nouns-a-n.txt
    tests/test_nouns-e-fa.txt
    tests/test_nouns-i-f0.txt
    tests/test_nouns_book.txt
    tests/test_nouns_wts.txt
    tests/test_pronouns.txt
    tests/test_verbs-AOR-IMP.txt
    tests/test_verbs-PRE.txt
    tests/test_verbs_ad.txt
    tests/test_verbs_base_pre.txt
    tests/test_verbs_book.txt
    tests/test_verbs_freq.txt
    tests/test_verbs_type.txt
    tests/testing_w_br-out.zip
    tests/testing_w_br.out
    tests/testing_w_br.txt

            
    new lines::
        tr -d '\015' < test_adj.txt                 > new_test_adj.txt
        mv new_test_adj.txt              test_adj.txt

    iteritems -> items

    :%s/print \(.*\)/print(\1)/gc 

    :%s/ u'/ '/gc
    :%s/(u'/('/gc
    :%s/\[u'/['/gc
    :%s/u'/'/gc  # paziti

    :%s/ u"/ "/gc
    :%s/(u"/("/gc
    :%s/\[u"/["/gc
    :%s/u"/"/gc  # paziti


    grep uni_revert
    grep to_unicode
    grep iteritems

    grep "print "
    grep "coding:"
    grep "utf"
    grep unicode, ... to_unicode ...
