def test_all():
    """browses all files in current dir and if .py then if has test attr then runs doctest, 
     if test_*.txt then run doctest. It skips itself
     NOTE: copied to euinfo.utils.test_curdir
     """
    import os
    import doctest
    import text_hr

    text_hr_root        = os.path.dirname(text_hr.__file__)
    this_root           = os.path.dirname(os.path.abspath(__file__))
    text_hr_root_should = os.path.join(os.path.dirname(this_root), "text_hr")
    if text_hr_root.lower()!=text_hr_root_should.lower():
        print("WARNING: module text_hr should be loaded from %s, but is loaded from %s, do you have local installation?" 
                        % (text_hr_root_should, text_hr_root))
    module_list = {}
    txtfiles_list = []
    for root_dir in (this_root, text_hr_root):
        for fname in os.listdir(root_dir):
            # fname = fname.lower()
            fname_abs = os.path.abspath(os.path.join(root_dir, fname))
            if fname_abs.lower()==os.path.abspath(__file__).lower():
                # skip testing self - check with __name__ or __file__
                continue
            if fname.endswith(".py"):
                modulename = fname.replace(".py", "")
                m_root = __import__("text_hr.%s" % modulename)
                m = getattr(m_root, modulename)
                if hasattr(m, "test"):
                    assert modulename not in list(module_list.keys())
                    module_list[modulename] = m
            elif fname.startswith("test_") and fname.endswith(".txt"):
                txtfiles_list.append(fname_abs)
    modnames_sorted = list(module_list.keys())
    modnames_sorted.sort()

    # TEST:
    # print(modnames_sorted)
    # modnames_sorted = ["utils", "nouns", "verbs"]
    for modulename in modnames_sorted:
        #if modulename not in ("detect", "nouns"):
        #    continue
        print("testing module   %s" % modulename)
        # m.test()
        #import doctest
        doctest.testmod(module_list[modulename])
        #del module_list[modulename]
        #del doctest
    # REMOVE_THIS:
    # return

    txtfiles_list.sort()
    
    # TEST: 
    # txtfiles_list = [
    #         # "/Users/rlujo/env/text-hr/text-hr/tests/test_adj.txt",
    #         "/Users/rlujo/env/text-hr/text-hr/tests/test_detect.txt",
    #         ]
    for fname in txtfiles_list:
        #if fname not in ("test_detect.txt", ):
        #    continue
        print("testing textfile %s" % fname)
        os.chdir(os.path.dirname(fname))
        fname_nodir = os.path.basename(fname)
        #import doctest
        doctest.testfile(fname_nodir)

        #del doctest

if __name__ == "__main__":
    test_all()

