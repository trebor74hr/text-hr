﻿
>>> from text_hr.verbs import *

>>> forms, words = VERBS.get_suffixes("V##AOR/o/").get_forms("doći", "dođ")
>>> print(VERBS.get_suffixes("V##AOR/o/").pp_forms(forms))
S/1       = ['do\u0111oh']
S/2       = ['do\u0111e']
S/3       = ['do\u0111e']
P/1       = ['do\u0111osmo']
P/2       = ['do\u0111oste']
P/3       = ['do\u0111o\u0161e']

Test palatalizacije 2 i 3 l.j.
>>> forms, words = VERBS.get_suffixes("V##AOR/o/").get_forms("reći", "rek")
>>> print(VERBS.get_suffixes("V##AOR/o/").pp_forms(forms))
S/1       = ['rekoh']
S/2       = ['re\u010de']
S/3       = ['re\u010de']
P/1       = ['rekosmo']
P/2       = ['rekoste']
P/3       = ['reko\u0161e']

>>> forms, words = VERBS.get_suffixes("V##AOR/o/").get_forms("stići", "stig")
>>> print(VERBS.get_suffixes("V##AOR/o/").pp_forms(forms))
S/1       = ['stigoh']
S/2       = ['sti\u017ee']
S/3       = ['sti\u017ee']
P/1       = ['stigosmo']
P/2       = ['stigoste']
P/3       = ['stigo\u0161e']

>>> forms, words = VERBS.get_suffixes("V##AOR//").get_forms("kazati", "kaza")
>>> print(VERBS.get_suffixes("V##AOR//").pp_forms(forms))
S/1       = ['kazah']
S/2       = ['kaza']
S/3       = ['kaza']
P/1       = ['kazasmo']
P/2       = ['kazaste']
P/3       = ['kaza\u0161e']

>>> print(VERBS.std_words["biti"].get_forms("V#AOR#").pp_forms())
S/1       = ['bih']
S/2       = ['bi']
S/3       = ['bi']
P/1       = ['bismo']
P/2       = ['biste']
P/3       = ['bi\u0161e', 'bi']

>>> print(VERBS.std_words["htjeti"].get_forms("V#AOR#").pp_forms())
S/1       = ['htjedoh']
S/2       = ['htjede']
S/3       = ['htjede']
P/1       = ['htjedosmo']
P/2       = ['htjedoste']
P/3       = ['htjedo\u0161e']

>>> lexem, ext, lexem_in, lexem_rule   = VERBS.suggest_lexem("vikati")[0]
>>> forms, words = VERBS.get_suffixes("V##IMP//").get_forms("vikati", lexem)
>>> print(VERBS.get_suffixes("V##IMP//").pp_forms(forms))
S/1       = ['vikah']
S/2       = ['vika\u0161e']
S/3       = ['vika\u0161e']
P/1       = ['vikasmo']
P/2       = ['vikaste']
P/3       = ['vikahu']

>>> lexem, ext, lexem_in, lexem_rule   = VERBS.suggest_lexem("čuti")[0]
>>> forms, words = VERBS.get_suffixes("V##IMP/j/").get_forms("čuti", lexem)
>>> print(VERBS.get_suffixes("V##IMP/j/").pp_forms(forms))
S/1       = ['\u010dujah']
S/2       = ['\u010duja\u0161e']
S/3       = ['\u010duja\u0161e']
P/1       = ['\u010dujasmo']
P/2       = ['\u010dujaste']
P/3       = ['\u010dujahu']

>>> lexem, ext, lexem_in, lexem_rule = VERBS.suggest_lexem("tresti")[0]
>>> forms, words = VERBS.get_suffixes("V##IMP/ij/").get_forms("tresti", lexem)
>>> print(VERBS.get_suffixes("V##IMP/ij/").pp_forms(forms))
S/1       = ['tresijah']
S/2       = ['tresija\u0161e']
S/3       = ['tresija\u0161e']
P/1       = ['tresijasmo']
P/2       = ['tresijaste']
P/3       = ['tresijahu']

>>> print(VERBS.std_words["biti"].get_forms("V#IMP#").pp_forms())
S/1       = ['bijah']
S/2       = ['bija\u0161e']
S/3       = ['bija\u0161e']
P/1       = ['bijasmo']
P/2       = ['bijaste']
P/3       = ['bijahu']

>>> print(VERBS.std_words["htjeti"].get_forms("V#IMP#").pp_forms())
S/1       = ['htijah']
S/2       = ['htija\u0161e']
S/3       = ['htija\u0161e']
P/1       = ['htijasmo']
P/2       = ['htijaste']
P/3       = ['htijahu']

>>> lexem, ext, lexem_in, lexem_rule = VERBS.suggest_lexem("brojati")[0]
>>> forms, words = VERBS.get_suffixes("V##IMV//").get_forms("brojati", lexem)
>>> print(VERBS.get_suffixes("V##IMV//").pp_forms(forms))
S/1       = ['']
S/2       = ['broj', 'broji']
S/3       = ['']
P/1       = ['brojmo']
P/2       = ['brojte']
P/3       = ['']

TODO: move this code to somewhere else - morphs.py probably
Test suffix code adjustment - selects like if only one:
>>> s1 = VERBS.get_suffixes("IMV#b")
>>> print(s1)
V#IMV#biti/-i(Broj/Osoba, 6 suffixes, based_on=V##IMV/i/)
>>> s2 = VERBS.get_suffixes("V#IMV#b")
>>> s1==s2
True

>>> lexem, ext, lexem_in, lexem_rule  = VERBS.suggest_lexem("nositi")[0]
>>> forms, words = VERBS.get_suffixes("V##IMV/i/").get_forms("nositi", lexem)
>>> print(VERBS.get_suffixes("V##IMV/i/").pp_forms(forms))
S/1       = ['']
S/2       = ['nosi']
S/3       = ['']
P/1       = ['nosimo']
P/2       = ['nosite']
P/3       = ['']

Suffix/forms key can be adjusted IMV -> V#IMV#
>>> print(VERBS.std_words["biti"].get_forms("IMV").pp_forms())
S/1       = ['']
S/2       = ['budi']
S/3       = ['bude']
P/1       = ['budimo']
P/2       = ['budite']
P/3       = ['budu']
