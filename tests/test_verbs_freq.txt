﻿
>>> from text_hr.verbs import *
>>> print(VERBS.std_words["morati"].get_forms("PRE").pp_forms())
S/1       = ['moram']
S/2       = ['mora\u0161']
S/3       = ['mora']
P/1       = ['moramo']
P/2       = ['morate']
P/3       = ['moraju']

>>> print(VERBS.std_words["smjeti"].get_forms("PRE").pp_forms())
S/1       = ['smijem']
S/2       = ['smije\u0161']
S/3       = ['smije']
P/1       = ['smijemo']
P/2       = ['smijete']
P/3       = ['smiju']

>>> print(VERBS.std_words["trebati"].get_forms("PRE").pp_forms())
S/1       = ['trebam']
S/2       = ['treba\u0161']
S/3       = ['treba']
P/1       = ['trebamo']
P/2       = ['trebate']
P/3       = ['trebaju']

>>> print(VERBS.std_words[u"mo\u0107i"].get_forms("PRE").pp_forms())
S/1       = ['mogu']
S/2       = ['mo\u017ee\u0161']
S/3       = ['mo\u017ee']
P/1       = ['mo\u017eemo']
P/2       = ['mo\u017eete']
P/3       = ['mo\u017eu']

>>> print(VERBS.std_words[u"\u017eeljeti"].get_forms("PRE").pp_forms())
S/1       = ['\u017eelim']
S/2       = ['\u017eeli\u0161']
S/3       = ['\u017eeli']
P/1       = ['\u017eelimo']
P/2       = ['\u017eelite']
P/3       = ['\u017eele']

>>> print(VERBS.std_words["morati"].get_forms("AOR").pp_forms())
S/1       = ['morah']
S/2       = ['mora']
S/3       = ['mora']
P/1       = ['morasmo']
P/2       = ['moraste']
P/3       = ['mora\u0161e']
>>> print(VERBS.std_words["trebati"].get_forms("AOR").pp_forms())
S/1       = ['trebah']
S/2       = ['treba']
S/3       = ['treba']
P/1       = ['trebasmo']
P/2       = ['trebaste']
P/3       = ['treba\u0161e']
>>> print(VERBS.std_words["smjeti"].get_forms("AOR").pp_forms())
S/1       = ['smijeh']
S/2       = ['smije']
S/3       = ['smije']
P/1       = ['smijesmo']
P/2       = ['smijeste']
P/3       = ['smije\u0161e']
>>> print(VERBS.std_words[u"mo\u0107i"].get_forms("AOR").pp_forms())
S/1       = ['mogah']
S/2       = ['moga']
S/3       = ['moga']
P/1       = ['mogasmo']
P/2       = ['mogaste']
P/3       = ['moga\u0161e']
>>> print(VERBS.std_words[u"\u017eeljeti"].get_forms("AOR").pp_forms())
S/1       = ['\u017eeljah']
S/2       = ['\u017eelja']
S/3       = ['\u017eelja']
P/1       = ['\u017eeljasmo']
P/2       = ['\u017eeljaste']
P/3       = ['\u017eelja\u0161e']

>>> print(VERBS.std_words[u"morati"].get_forms("V#VA_A#").pp_forms())
S/M       = ['morao']
S/F       = ['morala']
S/N       = ['moralo']
P/M       = ['morali']
P/F       = ['morale']
P/N       = ['morala']

>>> print(VERBS.std_words[u"trebati"].get_forms("V#VA_A#").pp_forms())
S/M       = ['trebao']
S/F       = ['trebala']
S/N       = ['trebalo']
P/M       = ['trebali']
P/F       = ['trebale']
P/N       = ['trebala']

#TODO: Moći has nepostojano A - mogala -> mogla, how to do this - it is in base+suffix part
>>> print(VERBS.std_words['mo\u0107i'].get_forms("V#VA_A#").pp_forms())
S/M       = ['mogao']
S/F       = ['mogala']
S/N       = ['mogalo']
P/M       = ['mogali']
P/F       = ['mogale']
P/N       = ['mogala']

>>> print(VERBS.std_words[u"\u017eeljeti"].get_forms("V#VA_P#").pp_forms())
S/M       = ['\u017eeljen']
S/F       = ['\u017eeljena']
S/N       = ['\u017eeljeno']
P/M       = ['\u017eeljeni']
P/F       = ['\u017eeljene']
P/N       = ['\u017eeljena']

# TODO: željeo -> želio # provjeriti zašto ne radi
>>> print(VERBS.std_words[u"\u017eeljeti"].get_forms("V#VA_A#").pp_forms())
S/M       = ['\u017eeljeo']
S/F       = ['\u017eeljela']
S/N       = ['\u017eeljelo']
P/M       = ['\u017eeljeli']
P/F       = ['\u017eeljele']
P/N       = ['\u017eeljela']

>>> print(VERBS.std_words[u"\u017eeljeti"].get_forms("PRE").pp_forms())
S/1       = ['\u017eelim']
S/2       = ['\u017eeli\u0161']
S/3       = ['\u017eeli']
P/1       = ['\u017eelimo']
P/2       = ['\u017eelite']
P/3       = ['\u017eele']


# >>> print(VERBS.std_words["morati"].get_forms("AOR").pp_forms())
# >>> print(VERBS.std_words["trebati"].get_forms("AOR").pp_forms())
# >>> print(VERBS.std_words["smjeti"].get_forms("AOR").pp_forms())
# >>> print(VERBS.std_words[u"mo\u0107i"].get_forms("AOR").pp_forms())
# >>> print(VERBS.std_words[u"\u017eeljeti"].get_forms("AOR").pp_forms())
