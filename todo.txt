PyPi - Registration 
===================

Procedure: 
 - read http://wiki.python.org/moin/Distutils/Tutorial
 - write this setup.py
 - create new account at http://pypi.python.org
 - reply on confirmation mail
 - python setup.py register
 - http://pypi.python.org/pypi/text-hr/
 - python setup.py sdist
 - python setup.py sdist upload
 - check there: http://pypi.python.org/pypi/text-hr/


How to post new release:
 - increase version nr. 
 - python setup.py sdist upload 
    - creates source distribution in dist directory -
      tarball and uploads it
 
TODO: 
    - run tran and fix problems
    - deploy - text-sentence
    - refactor?? - verbType -> Verb class props - like django Model
    - where is documentation?
        - sphinx and rest
        - explain .suff_registry.pickle
        - master thesis final ... on Croatian
        - documentation upload to pypi - make it with sphinx, zip and upload

    - refactor - extract hr data, make it lang. independent
    - write todo-s from dnevnik.txt
        -  

    - enable running tests like this::

        python -m"text_hr.verbs"


NOTES:
 - text-hr is included in softpedia db (linux):

    http://linux.softpedia.com/get/Text-Editing-Processing/Indexing/text-hr-57424.shtml

Lemmatization engine
====================
text-hr treba više shvatiti kao engine kojeg treba puniti tekstom, a kao
rezultat se dobiva baza riječi:

    - osnovni oblik, vrsta riječi, podvrsta i ostali atributi, te "algoritam"
      izvođenja u druge oblike (tip deklinacije ili sl.)

    - na osnovu "algoritma" se dobiju svi oblici te riječi, a za svaki oblik
      prema ulaznom tekstu dobijemo i učestalost svakog oblika

Kad se dobije dovoljno velika takva baza riječi onda se može raditi
lematizacija na vrlo jednostavan način: nađi oblik riječi u bazi -> dohvati
njen osnovni oblik (normiranje). Tu postoji određeni problem: da se ne nađe
oblik riječi (tj. nepoznat nam je), pa stoga bi trebalo za njega pokušati
naknadno obaviti prepoznavanje i tako pokušati nadopuniti bazu riječi s novim
riječima (inkrementalni sistem).

Stemming engine
===============
Drugi sistem koji bi se dao implementirati za potrebe normiranja riječi
(svođenje raznih oblika iste riječi na jedan - zovimo ga osnovni - oblik
riječi) je neka vrsta algoritma za stemming. Na osnovu baze riječi koju smo
dobili, može se nekim statističkim i/ili machine-learning metodama pokušati
izvući pravila za stemming. Npr. ako je riječ dulja od 6 znakova i završava na
<samoglasnik>+li onda je 90% vjerojatno da se radi o glagolu, a osnovni oblik
se dobiva da se pokuša invertirati glasnovna promjena na spoju s "li", makne
"li" i doda "ti" (pisali -> pisati). Dakle isti algoritam koji ja primjenjujem
kod izvođenja riječi u njene oblike, samo u suprotnom smjeru.  

To je malo zahtjevno, no mislim da je na kraju dovoljno dobar sistem koji bi
bio otporniji na riječi koje nije vidio, memorijski manje zahtjevan, no cpu
zahtjevniji.


primjer slične ideje
--------------------
Enes: jednostavan model koji reže određeni broj zadnjih slova za riječi iznad
određenog broja znakova i miče samoglasnik ako se nalazi kao zadnje slovo
riječi. Uz optimizaciju se može dobiti optimalan broj slova koje treba
odrezati, tako da sam na kraju uspio dobiti dobar model, iako sam se služio
ovako grubim načinom. Ipak iz tvog koda sam uspio izvuči zaustavne riječi, što
je bilo od pomoći, a na kraju sam metodama strojnog učenja uspio dobiti solidan
rezultat.

Korisni linkovi:
    - http://nlp.ffzg.hr/
    - http://faust.ffzg.hr/nlpws/gui/

