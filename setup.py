﻿import setuptools

# ---------------------
# DEPLOYMENT PROCEDURE 
# ---------------------
#
# reference: https://packaging.python.org/tutorials/packaging-projects/
#
#   - upgrade/install setuptools and wheel:
#       python3 -m pip install --upgrade setuptools wheel
#   - update README, update RELEASES section
#   - increase version in this file
#   - commit changes, push and mark commit in bitbucket with tag containing
#     version number:
#       https://bitbucket.org/trebor74hr/text-hr/commits/
#   - create wheel and src dist:
#       python setup.py sdist bdist_wheel

#   - create account and token if not already: https://test.pypi.org/
#   - upgrade pypi:
#       python3 -m pip install --user --upgrade twine
#   - deploy to test pypi:
#       python3 -m twine upload --repository testpypi dist/*
#       Set your username to __token__
#       Set your password to the token value, including the "pypi-" prefix - 
#       
#     if everything is ok, you'll get: something like
#       View at: https://test.pypi.org/project/text-hr/0.20/

#   - test deployment:
#       cd ~/env/text-hr/
#       rm -R py-test
#       python3 -m venv py-test
#       . ./py-test/bin/activate
#       python3 -m pip install --index-url https://test.pypi.org/simple --no-deps text-hr
#           Looking in indexes: https://test.pypi.org/simple
#           Collecting text-hr
#             Downloading https://test-files.pythonhosted.org/packages/...
#                |████████████████████████████████| 102kB 1.6MB/s
#           Installing collected packages: text-hr
#           Successfully installed text-hr-0.20
#
#       # test if it works:
#       python 
#       >>> import text_hr; print(text_hr.__file__)
#       ~/env/text-hr/py-test/lib/python3.7/site-packages/text_hr/__init__.py
#       >>> from text_hr import verbs; print(verbs.AEIOU)

#   - real deployment:: 
#       python3 -m twine upload dist/* 
#
#   - test once more - see test deployment
#       cd ~/env/text-hr/
#       rm -R py-test
#       python3 -m venv py-test
#       . ./py-test/bin/activate
#       python3 -m pip install --no-deps text-hr
#           Collecting text-hr
#             Downloading https://files.pythonhosted.org/packages/...
#                |████████████████████████████████| 102kB 3.6MB/s
#           Installing collected packages: text-hr
#           Successfully installed text-hr-0.20
#       # test if it works:
#       python 
#       >>> import text_hr; print(text_hr.__file__)
#       ~/env/text-hr/py-test/lib/python3.7/site-packages/text_hr/__init__.py
#       >>> from text_hr import verbs; print(verbs.AEIOU)
#
#
# TODO: put in __init__.py variable __VERSION__ to match current version

with open("README", "r") as fh:
    long_description = fh.read()

files = ["text_hr/std_words.txt"]

setuptools.setup(
    name='text-hr',
    version='0.20',
    #Name the folder where your packages live:
    packages = ['text_hr'],
    # TODO: packages=setuptools.find_packages(),

    #'package' package must contain files (see list above)
    #It says, package *needs* these files.
    package_data = {'text_hr' : files },
    description = "Morphological/Inflection/Lemmatization Engine for Croatian language, POS tagger, stopwords",
    author = "Robert Lujo",
    author_email = "trebor74hr@gmail.com",
    url = "http://bitbucket.org/trebor74hr/text-hr/",
    #'runner' is in the root.
    # scripts = ["runner"],
    long_description = long_description,
    # TODO: ?? long_description_content_type="text/markdown",
    # TODO: enable hr chars in char before

    # This next part it for the Cheese Shop
    # http://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 4 - Beta',
        "Programming Language :: Python :: 3",
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Education',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Natural Language :: Croatian',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Text Processing',
        'Topic :: Internet :: WWW/HTTP :: Indexing/Search',
        'Topic :: Scientific/Engineering :: Information Analysis',
        'Topic :: Text Processing :: Linguistic',
        'Topic :: Text Processing :: Indexing',
      ],
    python_requires='>=3.3',
    )
